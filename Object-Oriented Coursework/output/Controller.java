import java.util.List;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

import java.lang.*;
import java.lang.reflect.*;
import java.util.StringTokenizer;
import java.io.*;



class Staff
  implements SystemTypes
{
  private String staffId = ""; // internal
  private int costDay = 0; // internal
  private List assigned = new Vector(); // of Assignment
  private List has = new Vector(); // of Skill

  public Staff()
  {
    this.staffId = "";
    this.costDay = 0;

  }



  public String toString()
  { String _res_ = "(Staff) ";
    _res_ = _res_ + staffId + ",";
    _res_ = _res_ + costDay;
    return _res_;
  }

  public void setstaffId(String staffId_x) { staffId = staffId_x;  }


    public static void setAllstaffId(List staffs,String val)
  { for (int i = 0; i < staffs.size(); i++)
    { Staff staffx = (Staff) staffs.get(i);
      Controller.inst().setstaffId(staffx,val); } }


  public void setcostDay(int costDay_x) { costDay = costDay_x;  }


    public static void setAllcostDay(List staffs,int val)
  { for (int i = 0; i < staffs.size(); i++)
    { Staff staffx = (Staff) staffs.get(i);
      Controller.inst().setcostDay(staffx,val); } }


  public void setassigned(List assignedxx) { assigned = assignedxx;
    }
 
  public void addassigned(Assignment assignedxx) { assigned.add(assignedxx);
    }
 
  public void removeassigned(Assignment assignedxx) { Vector _removedassignedassignedxx = new Vector();
  _removedassignedassignedxx.add(assignedxx);
  assigned.removeAll(_removedassignedassignedxx);
    }

  public static void setAllassigned(List staffs,List _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().setassigned(staffx, _val); } }

  public static void addAllassigned(List staffs,Assignment _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().addassigned(staffx, _val); } }


  public static void removeAllassigned(List staffs,Assignment _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().removeassigned(staffx, _val); } }


  public static void unionAllassigned(List staffs, List _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().unionassigned(staffx, _val); } }


  public static void subtractAllassigned(List staffs, List _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().subtractassigned(staffx, _val); } }


  public void sethas(List hasxx) { has = hasxx;
    }
 
  public void addhas(Skill hasxx) { has.add(hasxx);
    }
 
  public void removehas(Skill hasxx) { Vector _removedhashasxx = new Vector();
  _removedhashasxx.add(hasxx);
  has.removeAll(_removedhashasxx);
    }

  public static void setAllhas(List staffs,List _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().sethas(staffx, _val); } }

  public static void addAllhas(List staffs,Skill _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().addhas(staffx, _val); } }


  public static void removeAllhas(List staffs,Skill _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().removehas(staffx, _val); } }


  public static void unionAllhas(List staffs, List _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().unionhas(staffx, _val); } }


  public static void subtractAllhas(List staffs, List _val)
  { for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      Controller.inst().subtracthas(staffx, _val); } }


    public String getstaffId() { return staffId; }

    public static List getAllstaffId(List staffs)
  { List result = new Vector();
    for (int i = 0; i < staffs.size(); i++)
    { Staff staffx = (Staff) staffs.get(i);
      if (result.contains(staffx.getstaffId())) { }
      else { result.add(staffx.getstaffId()); } }
    return result; }

    public static List getAllOrderedstaffId(List staffs)
  { List result = new Vector();
    for (int i = 0; i < staffs.size(); i++)
    { Staff staffx = (Staff) staffs.get(i);
      result.add(staffx.getstaffId()); } 
    return result; }

    public int getcostDay() { return costDay; }

    public static List getAllcostDay(List staffs)
  { List result = new Vector();
    for (int i = 0; i < staffs.size(); i++)
    { Staff staffx = (Staff) staffs.get(i);
      if (result.contains(new Integer(staffx.getcostDay()))) { }
      else { result.add(new Integer(staffx.getcostDay())); } }
    return result; }

    public static List getAllOrderedcostDay(List staffs)
  { List result = new Vector();
    for (int i = 0; i < staffs.size(); i++)
    { Staff staffx = (Staff) staffs.get(i);
      result.add(new Integer(staffx.getcostDay())); } 
    return result; }

  public List getassigned() { return (Vector) ((Vector) assigned).clone(); }

  public static List getAllassigned(List staffs)
  { List result = new Vector();
    for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      result = Set.union(result,staffx.getassigned()); }
    return result; }

  public static List getAllOrderedassigned(List staffs)
  { List result = new Vector();
    for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      result = Set.union(result,staffx.getassigned()); }
    return result; }

  public List gethas() { return (Vector) ((Vector) has).clone(); }

  public static List getAllhas(List staffs)
  { List result = new Vector();
    for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      result = Set.union(result,staffx.gethas()); }
    return result; }

  public static List getAllOrderedhas(List staffs)
  { List result = new Vector();
    for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx = (Staff) staffs.get(_i);
      result = Set.union(result,staffx.gethas()); }
    return result; }


}


class Assignment
  implements SystemTypes
{
  private Staff staff;
  private Task task;

  public Assignment(Staff staff,Task task)
  {
    this.staff = staff;
    this.task = task;

  }

  public Assignment() { }



  public void setstaff(Staff staffxx) { staff = staffxx;
  }

  public static void setAllstaff(List assignments,Staff _val)
  { for (int _i = 0; _i < assignments.size(); _i++)
    { Assignment assignmentx = (Assignment) assignments.get(_i);
      Controller.inst().setstaff(assignmentx, _val); } }

  public void settask(Task taskxx) { task = taskxx;
  }

  public static void setAlltask(List assignments,Task _val)
  { for (int _i = 0; _i < assignments.size(); _i++)
    { Assignment assignmentx = (Assignment) assignments.get(_i);
      Controller.inst().settask(assignmentx, _val); } }

  public Staff getstaff() { return staff; }

  public static List getAllstaff(List assignments)
  { List result = new Vector();
    for (int _i = 0; _i < assignments.size(); _i++)
    { Assignment assignmentx = (Assignment) assignments.get(_i);
      if (result.contains(assignmentx.getstaff())) {}
      else { result.add(assignmentx.getstaff()); }
 }
    return result; }

  public static List getAllOrderedstaff(List assignments)
  { List result = new Vector();
    for (int _i = 0; _i < assignments.size(); _i++)
    { Assignment assignmentx = (Assignment) assignments.get(_i);
      if (result.contains(assignmentx.getstaff())) {}
      else { result.add(assignmentx.getstaff()); }
 }
    return result; }

  public Task gettask() { return task; }

  public static List getAlltask(List assignments)
  { List result = new Vector();
    for (int _i = 0; _i < assignments.size(); _i++)
    { Assignment assignmentx = (Assignment) assignments.get(_i);
      if (result.contains(assignmentx.gettask())) {}
      else { result.add(assignmentx.gettask()); }
 }
    return result; }

  public static List getAllOrderedtask(List assignments)
  { List result = new Vector();
    for (int _i = 0; _i < assignments.size(); _i++)
    { Assignment assignmentx = (Assignment) assignments.get(_i);
      if (result.contains(assignmentx.gettask())) {}
      else { result.add(assignmentx.gettask()); }
 }
    return result; }

    public String toString()
  {   String result = "";
 
  result = task.gettaskId() + ", " + task.getduration() + ", " + staff.getstaffId() + ", " + staff.getcostDay();
    return result;
  }


    public void displayschedule1()
  {   System.out.println("" + this);

  }


}


class Task
  implements SystemTypes
{
  private String taskId = ""; // internal
  private int duration = 0; // internal
  private List assignment = new Vector(); // of Assignment
  private List dependsOn = new Vector(); // of Task
  private List needs = new Vector(); // of Skill

  public Task()
  {
    this.taskId = "";
    this.duration = 0;

  }



  public String toString()
  { String _res_ = "(Task) ";
    _res_ = _res_ + taskId + ",";
    _res_ = _res_ + duration;
    return _res_;
  }

  public void settaskId(String taskId_x) { taskId = taskId_x;  }


    public static void setAlltaskId(List tasks,String val)
  { for (int i = 0; i < tasks.size(); i++)
    { Task taskx = (Task) tasks.get(i);
      Controller.inst().settaskId(taskx,val); } }


  public void setduration(int duration_x) { duration = duration_x;  }


    public static void setAllduration(List tasks,int val)
  { for (int i = 0; i < tasks.size(); i++)
    { Task taskx = (Task) tasks.get(i);
      Controller.inst().setduration(taskx,val); } }


  public void setassignment(List assignmentxx) { assignment = assignmentxx;
    }
 
  public void addassignment(Assignment assignmentxx) { assignment.add(assignmentxx);
    }
 
  public void removeassignment(Assignment assignmentxx) { Vector _removedassignmentassignmentxx = new Vector();
  _removedassignmentassignmentxx.add(assignmentxx);
  assignment.removeAll(_removedassignmentassignmentxx);
    }

  public static void setAllassignment(List tasks,List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().setassignment(taskx, _val); } }

  public static void addAllassignment(List tasks,Assignment _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().addassignment(taskx, _val); } }


  public static void removeAllassignment(List tasks,Assignment _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().removeassignment(taskx, _val); } }


  public static void unionAllassignment(List tasks, List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().unionassignment(taskx, _val); } }


  public static void subtractAllassignment(List tasks, List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().subtractassignment(taskx, _val); } }


  public void setdependsOn(List dependsOnxx) { dependsOn = dependsOnxx;
    }
 
  public void adddependsOn(Task dependsOnxx) { dependsOn.add(dependsOnxx);
    }
 
  public void removedependsOn(Task dependsOnxx) { Vector _removeddependsOndependsOnxx = new Vector();
  _removeddependsOndependsOnxx.add(dependsOnxx);
  dependsOn.removeAll(_removeddependsOndependsOnxx);
    }

  public static void setAlldependsOn(List tasks,List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().setdependsOn(taskx, _val); } }

  public static void addAlldependsOn(List tasks,Task _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().adddependsOn(taskx, _val); } }


  public static void removeAlldependsOn(List tasks,Task _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().removedependsOn(taskx, _val); } }


  public static void unionAlldependsOn(List tasks, List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().uniondependsOn(taskx, _val); } }


  public static void subtractAlldependsOn(List tasks, List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().subtractdependsOn(taskx, _val); } }


  public void setneeds(List needsxx) { needs = needsxx;
    }
 
  public void addneeds(Skill needsxx) { needs.add(needsxx);
    }
 
  public void removeneeds(Skill needsxx) { Vector _removedneedsneedsxx = new Vector();
  _removedneedsneedsxx.add(needsxx);
  needs.removeAll(_removedneedsneedsxx);
    }

  public static void setAllneeds(List tasks,List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().setneeds(taskx, _val); } }

  public static void addAllneeds(List tasks,Skill _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().addneeds(taskx, _val); } }


  public static void removeAllneeds(List tasks,Skill _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().removeneeds(taskx, _val); } }


  public static void unionAllneeds(List tasks, List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().unionneeds(taskx, _val); } }


  public static void subtractAllneeds(List tasks, List _val)
  { for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      Controller.inst().subtractneeds(taskx, _val); } }


    public String gettaskId() { return taskId; }

    public static List getAlltaskId(List tasks)
  { List result = new Vector();
    for (int i = 0; i < tasks.size(); i++)
    { Task taskx = (Task) tasks.get(i);
      if (result.contains(taskx.gettaskId())) { }
      else { result.add(taskx.gettaskId()); } }
    return result; }

    public static List getAllOrderedtaskId(List tasks)
  { List result = new Vector();
    for (int i = 0; i < tasks.size(); i++)
    { Task taskx = (Task) tasks.get(i);
      result.add(taskx.gettaskId()); } 
    return result; }

    public int getduration() { return duration; }

    public static List getAllduration(List tasks)
  { List result = new Vector();
    for (int i = 0; i < tasks.size(); i++)
    { Task taskx = (Task) tasks.get(i);
      if (result.contains(new Integer(taskx.getduration()))) { }
      else { result.add(new Integer(taskx.getduration())); } }
    return result; }

    public static List getAllOrderedduration(List tasks)
  { List result = new Vector();
    for (int i = 0; i < tasks.size(); i++)
    { Task taskx = (Task) tasks.get(i);
      result.add(new Integer(taskx.getduration())); } 
    return result; }

  public List getassignment() { return (Vector) ((Vector) assignment).clone(); }

  public static List getAllassignment(List tasks)
  { List result = new Vector();
    for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      result = Set.union(result,taskx.getassignment()); }
    return result; }

  public static List getAllOrderedassignment(List tasks)
  { List result = new Vector();
    for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      result = Set.union(result,taskx.getassignment()); }
    return result; }

  public List getdependsOn() { return (Vector) ((Vector) dependsOn).clone(); }

  public static List getAlldependsOn(List tasks)
  { List result = new Vector();
    for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      result = Set.union(result,taskx.getdependsOn()); }
    return result; }

  public static List getAllOrdereddependsOn(List tasks)
  { List result = new Vector();
    for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      result = Set.union(result,taskx.getdependsOn()); }
    return result; }

  public List getneeds() { return (Vector) ((Vector) needs).clone(); }

  public static List getAllneeds(List tasks)
  { List result = new Vector();
    for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      result = Set.union(result,taskx.getneeds()); }
    return result; }

  public static List getAllOrderedneeds(List tasks)
  { List result = new Vector();
    for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx = (Task) tasks.get(_i);
      result = Set.union(result,taskx.getneeds()); }
    return result; }

    public void allocatestaff1(Staff st)
  {   //  if ((!(assignment.size() == 0) || st.getassigned().size() != 0))) { return; } 
  Assignment a = new Assignment();
    Controller.inst().addAssignment(a);
    Controller.inst().settask(a,this);
    Controller.inst().setstaff(a,st);
  }

    public boolean allocatestaff1test(Staff st)
  {  boolean result;
Task taskx = this;
     if (taskx.getassignment().size() == 0 && st.getassigned().size() == 0) {   return true; }

    return false;

  }


    public static boolean allocatestaff1search()
  {  boolean result;
  List _range1 = Controller.inst().tasks;
  for (int _i0 = 0; _i0 < _range1.size(); _i0++)
  { Task taskx = (Task) _range1.get(_i0);
      List _range3 = Controller.inst().staffs;
  for (int _i2 = 0; _i2 < _range3.size(); _i2++)
  { Staff st = (Staff) _range3.get(_i2);
       if (taskx.allocatestaff1test(st)) {    Controller.inst().allocatestaff1(taskx,st);
    return true;
 }

  }
  }
    return false;

  }



}


class AllocateStaff
  implements SystemTypes
{

  public AllocateStaff()
  {

  }



  public String toString()
  { String _res_ = "(AllocateStaff) ";
    return _res_;
  }


}


class DisplaySchedule
  implements SystemTypes
{

  public DisplaySchedule()
  {

  }



  public String toString()
  { String _res_ = "(DisplaySchedule) ";
    return _res_;
  }


}


class Story
  implements SystemTypes
{
  private String storyId = ""; // internal
  private List needs = new Vector(); // of Task

  public Story()
  {
    this.storyId = "";

  }



  public String toString()
  { String _res_ = "(Story) ";
    _res_ = _res_ + storyId;
    return _res_;
  }

  public void setstoryId(String storyId_x) { storyId = storyId_x;  }


    public static void setAllstoryId(List storys,String val)
  { for (int i = 0; i < storys.size(); i++)
    { Story storyx = (Story) storys.get(i);
      Controller.inst().setstoryId(storyx,val); } }


  public void setneeds(List needsxx) { needs = needsxx;
    }
 
  public void addneeds(Task needsxx) { needs.add(needsxx);
    }
 
  public void removeneeds(Task needsxx) { Vector _removedneedsneedsxx = new Vector();
  _removedneedsneedsxx.add(needsxx);
  needs.removeAll(_removedneedsneedsxx);
    }

  public static void setAllneeds(List storys,List _val)
  { for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx = (Story) storys.get(_i);
      Controller.inst().setneeds(storyx, _val); } }

  public static void addAllneeds(List storys,Task _val)
  { for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx = (Story) storys.get(_i);
      Controller.inst().addneeds(storyx, _val); } }


  public static void removeAllneeds(List storys,Task _val)
  { for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx = (Story) storys.get(_i);
      Controller.inst().removeneeds(storyx, _val); } }


  public static void unionAllneeds(List storys, List _val)
  { for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx = (Story) storys.get(_i);
      Controller.inst().unionneeds(storyx, _val); } }


  public static void subtractAllneeds(List storys, List _val)
  { for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx = (Story) storys.get(_i);
      Controller.inst().subtractneeds(storyx, _val); } }


    public String getstoryId() { return storyId; }

    public static List getAllstoryId(List storys)
  { List result = new Vector();
    for (int i = 0; i < storys.size(); i++)
    { Story storyx = (Story) storys.get(i);
      if (result.contains(storyx.getstoryId())) { }
      else { result.add(storyx.getstoryId()); } }
    return result; }

    public static List getAllOrderedstoryId(List storys)
  { List result = new Vector();
    for (int i = 0; i < storys.size(); i++)
    { Story storyx = (Story) storys.get(i);
      result.add(storyx.getstoryId()); } 
    return result; }

  public List getneeds() { return (Vector) ((Vector) needs).clone(); }

  public static List getAllneeds(List storys)
  { List result = new Vector();
    for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx = (Story) storys.get(_i);
      result = Set.union(result,storyx.getneeds()); }
    return result; }

  public static List getAllOrderedneeds(List storys)
  { List result = new Vector();
    for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx = (Story) storys.get(_i);
      result = Set.union(result,storyx.getneeds()); }
    return result; }


}


class Skill
  implements SystemTypes
{
  private String skillId = ""; // internal

  public Skill()
  {
    this.skillId = "";

  }



  public String toString()
  { String _res_ = "(Skill) ";
    _res_ = _res_ + skillId;
    return _res_;
  }

  public void setskillId(String skillId_x) { skillId = skillId_x;  }


    public static void setAllskillId(List skills,String val)
  { for (int i = 0; i < skills.size(); i++)
    { Skill skillx = (Skill) skills.get(i);
      Controller.inst().setskillId(skillx,val); } }


    public String getskillId() { return skillId; }

    public static List getAllskillId(List skills)
  { List result = new Vector();
    for (int i = 0; i < skills.size(); i++)
    { Skill skillx = (Skill) skills.get(i);
      if (result.contains(skillx.getskillId())) { }
      else { result.add(skillx.getskillId()); } }
    return result; }

    public static List getAllOrderedskillId(List skills)
  { List result = new Vector();
    for (int i = 0; i < skills.size(); i++)
    { Skill skillx = (Skill) skills.get(i);
      result.add(skillx.getskillId()); } 
    return result; }


}


class Schedule
  implements SystemTypes
{
  private int totalCost = 0; // internal
  private List Assignment = new Vector(); // of Assignment

  public Schedule()
  {
    this.totalCost = 0;

  }



  public String toString()
  { String _res_ = "(Schedule) ";
    _res_ = _res_ + totalCost;
    return _res_;
  }

  public void settotalCost(int totalCost_x) { totalCost = totalCost_x;  }


    public static void setAlltotalCost(List schedules,int val)
  { for (int i = 0; i < schedules.size(); i++)
    { Schedule schedulex = (Schedule) schedules.get(i);
      Controller.inst().settotalCost(schedulex,val); } }


  public void setAssignment(List Assignmentxx) { Assignment = Assignmentxx;
    }
 
  public void setAssignment(int ind_x,Assignment Assignmentxx) { Assignment.set(ind_x,Assignmentxx); }

 public void addAssignment(Assignment Assignmentxx) { Assignment.add(Assignmentxx);
    }
 
  public void removeAssignment(Assignment Assignmentxx) { Vector _removedAssignmentAssignmentxx = new Vector();
  _removedAssignmentAssignmentxx.add(Assignmentxx);
  Assignment.removeAll(_removedAssignmentAssignmentxx);
    }

  public static void setAllAssignment(List schedules,List _val)
  { for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      Controller.inst().setAssignment(schedulex, _val); } }

  public static void setAllAssignment(List schedules,int _ind,Assignment _val)
  { for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      Controller.inst().setAssignment(schedulex,_ind,_val); } }

  public static void addAllAssignment(List schedules,Assignment _val)
  { for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      Controller.inst().addAssignment(schedulex, _val); } }


  public static void removeAllAssignment(List schedules,Assignment _val)
  { for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      Controller.inst().removeAssignment(schedulex, _val); } }


  public static void unionAllAssignment(List schedules, List _val)
  { for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      Controller.inst().unionAssignment(schedulex, _val); } }


  public static void subtractAllAssignment(List schedules, List _val)
  { for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      Controller.inst().subtractAssignment(schedulex, _val); } }


    public int gettotalCost() { return totalCost; }

    public static List getAlltotalCost(List schedules)
  { List result = new Vector();
    for (int i = 0; i < schedules.size(); i++)
    { Schedule schedulex = (Schedule) schedules.get(i);
      if (result.contains(new Integer(schedulex.gettotalCost()))) { }
      else { result.add(new Integer(schedulex.gettotalCost())); } }
    return result; }

    public static List getAllOrderedtotalCost(List schedules)
  { List result = new Vector();
    for (int i = 0; i < schedules.size(); i++)
    { Schedule schedulex = (Schedule) schedules.get(i);
      result.add(new Integer(schedulex.gettotalCost())); } 
    return result; }

  public List getAssignment() { return (Vector) ((Vector) Assignment).clone(); }

  public static List getAllAssignment(List schedules)
  { List result = new Vector();
    for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      result = Set.union(result,schedulex.getAssignment()); }
    return result; }

  public static List getAllOrderedAssignment(List schedules)
  { List result = new Vector();
    for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex = (Schedule) schedules.get(_i);
      result.addAll(schedulex.getAssignment()); }
    return result; }


}


class Create
  implements SystemTypes
{

  public Create()
  {

  }



  public String toString()
  { String _res_ = "(Create) ";
    return _res_;
  }


}


class CalculateCost
  implements SystemTypes
{

  public CalculateCost()
  {

  }



  public String toString()
  { String _res_ = "(CalculateCost) ";
    return _res_;
  }


}



public class Controller implements SystemTypes, ControllerInterface
{
  Vector staffs = new Vector();
  Map staffstaffIdindex = new HashMap(); // String --> Staff

  Vector assignments = new Vector();
  Vector tasks = new Vector();
  Map tasktaskIdindex = new HashMap(); // String --> Task

  Vector allocatestaffs = new Vector();
  Vector displayschedules = new Vector();
  Vector storys = new Vector();
  Map storystoryIdindex = new HashMap(); // String --> Story

  Vector skills = new Vector();
  Map skillskillIdindex = new HashMap(); // String --> Skill

  Vector schedules = new Vector();
  Vector creates = new Vector();
  Vector calculatecosts = new Vector();
  private static Controller uniqueInstance; 


  private Controller() { } 


  public static Controller inst() 
    { if (uniqueInstance == null) 
    { uniqueInstance = new Controller(); }
    return uniqueInstance; } 


  public static void loadModel(String file)
  {
    try
    { BufferedReader br = null;
      File f = new File(file);
      try 
      { br = new BufferedReader(new FileReader(f)); }
      catch (Exception ex) 
      { System.err.println("No file: " + file); return; }
      Class cont = Class.forName("Controller");
      java.util.Map objectmap = new java.util.HashMap();
      while (true)
      { String line1;
        try { line1 = br.readLine(); }
        catch (Exception e)
        { return; }
        if (line1 == null)
        { return; }
        line1 = line1.trim();

        if (line1.length() == 0) { continue; }
        String left;
        String op;
        String right;
        if (line1.charAt(line1.length() - 1) == '"')
        { int eqind = line1.indexOf("="); 
          if (eqind == -1) { continue; }
          else 
          { left = line1.substring(0,eqind-1).trim();
            op = "="; 
            right = line1.substring(eqind+1,line1.length()).trim();
          }
        }
        else
        { StringTokenizer st1 = new StringTokenizer(line1);
          Vector vals1 = new Vector();
          while (st1.hasMoreTokens())
          { String val1 = st1.nextToken();
            vals1.add(val1);
          }
          if (vals1.size() < 3)
          { continue; }
          left = (String) vals1.get(0);
          op = (String) vals1.get(1);
          right = (String) vals1.get(2);
        }
        if (":".equals(op))
        { int i2 = right.indexOf(".");
          if (i2 == -1)
          { Class cl;
            try { cl = Class.forName("" + right); }
            catch (Exception _x) { System.err.println("No entity: " + right); continue; }
            Object xinst = cl.newInstance();
            objectmap.put(left,xinst);
            Class[] cargs = new Class[] { cl };
            Method addC = cont.getMethod("add" + right,cargs);
            if (addC == null) { continue; }
            Object[] args = new Object[] { xinst };
            addC.invoke(Controller.inst(),args);
          }
          else
          { String obj = right.substring(0,i2);
            String role = right.substring(i2+1,right.length());
            Object objinst = objectmap.get(obj); 
            if (objinst == null) 
            { continue; }
            Object val = objectmap.get(left);
            if (val == null) 
            { continue; }
            Class objC = objinst.getClass();
            Class typeclass = val.getClass(); 
            Object[] args = new Object[] { val }; 
            Class[] settypes = new Class[] { typeclass };
            Method addrole = Controller.findMethod(objC,"add" + role);
            if (addrole != null) 
            { addrole.invoke(objinst, args); }
            else { System.err.println("Error: cannot add to " + role); }
          }
        }
        else if ("=".equals(op))
        { int i1 = left.indexOf(".");
          if (i1 == -1) 
          { continue; }
          String obj = left.substring(0,i1);
          String att = left.substring(i1+1,left.length());
          Object objinst = objectmap.get(obj); 
          if (objinst == null) 
          { continue; }
          Class objC = objinst.getClass();
          Class typeclass; 
          Object val; 
          if (right.charAt(0) == '"' &&
              right.charAt(right.length() - 1) == '"')
          { typeclass = String.class;
            val = right.substring(1,right.length() - 1);
          } 
          else if ("true".equals(right) || "false".equals(right))
          { typeclass = boolean.class;
            if ("true".equals(right))
            { val = new Boolean(true); }
            else
            { val = new Boolean(false); }
          }
          else 
          { val = objectmap.get(right);
            if (val != null)
            { typeclass = val.getClass(); }
            else 
            { int i;
              long l; 
              double d;
              try 
              { i = Integer.parseInt(right);
                typeclass = int.class;
                val = new Integer(i); 
              }
              catch (Exception ee)
              { try 
                { l = Long.parseLong(right);
                  typeclass = long.class;
                  val = new Long(l); 
                }
                catch (Exception eee)
                { try
                  { d = Double.parseDouble(right);
                    typeclass = double.class;
                    val = new Double(d);
                  }
                  catch (Exception ff)
                  { continue; }
                }
              }
            }
          }
          Object[] args = new Object[] { val }; 
          Class[] settypes = new Class[] { typeclass };
          Method setatt = Controller.findMethod(objC,"set" + att);
          if (setatt != null) 
          { setatt.invoke(objinst, args); }
          else { System.err.println("No attribute: " + att); }
        }
      }
    } catch (Exception e) { }
  }

  public static Method findMethod(Class c, String name)
  { Method[] mets = c.getMethods(); 
    for (int i = 0; i < mets.length; i++)
    { Method m = mets[i];
      if (m.getName().equals(name))
      { return m; }
    } 
    return null;
  }


  public void checkCompleteness()
  {   for (int _i = 0; _i < staffs.size(); _i++)
  { Staff staff_x = (Staff) staffs.get(_i);
    Staff staff_obj = (Staff) staffstaffIdindex.get(staff_x.getstaffId());
    if (staff_obj == staff_x) { }
    else if (staff_obj == null)
    { staffstaffIdindex.put(staff_x.getstaffId(),staff_x); }
    else
    { System.out.println("Error: multiple objects with staffId = " + staff_x.getstaffId()); }
  }
  for (int _i = 0; _i < tasks.size(); _i++)
  { Task task_x = (Task) tasks.get(_i);
    Task task_obj = (Task) tasktaskIdindex.get(task_x.gettaskId());
    if (task_obj == task_x) { }
    else if (task_obj == null)
    { tasktaskIdindex.put(task_x.gettaskId(),task_x); }
    else
    { System.out.println("Error: multiple objects with taskId = " + task_x.gettaskId()); }
  }
  for (int _i = 0; _i < storys.size(); _i++)
  { Story story_x = (Story) storys.get(_i);
    Story story_obj = (Story) storystoryIdindex.get(story_x.getstoryId());
    if (story_obj == story_x) { }
    else if (story_obj == null)
    { storystoryIdindex.put(story_x.getstoryId(),story_x); }
    else
    { System.out.println("Error: multiple objects with storyId = " + story_x.getstoryId()); }
  }
  for (int _i = 0; _i < skills.size(); _i++)
  { Skill skill_x = (Skill) skills.get(_i);
    Skill skill_obj = (Skill) skillskillIdindex.get(skill_x.getskillId());
    if (skill_obj == skill_x) { }
    else if (skill_obj == null)
    { skillskillIdindex.put(skill_x.getskillId(),skill_x); }
    else
    { System.out.println("Error: multiple objects with skillId = " + skill_x.getskillId()); }
  }
  for (int _i = 0; _i < assignments.size(); _i++)
  { Assignment assigned_assignmentx1 = (Assignment) assignments.get(_i);
    for (int _j = 0; _j < staffs.size(); _j++)
    { Staff staff_staffx2 = (Staff) staffs.get(_j);
      if (assigned_assignmentx1.getstaff() == staff_staffx2)
      { if (staff_staffx2.getassigned().contains(assigned_assignmentx1)) { }
        else { staff_staffx2.addassigned(assigned_assignmentx1); }
      }
      else if (staff_staffx2.getassigned().contains(assigned_assignmentx1))
      { assigned_assignmentx1.setstaff(staff_staffx2); } 
    }
  }
  for (int _i = 0; _i < assignments.size(); _i++)
  { Assignment assignment_assignmentx1 = (Assignment) assignments.get(_i);
    for (int _j = 0; _j < tasks.size(); _j++)
    { Task task_taskx2 = (Task) tasks.get(_j);
      if (assignment_assignmentx1.gettask() == task_taskx2)
      { if (task_taskx2.getassignment().contains(assignment_assignmentx1)) { }
        else { task_taskx2.addassignment(assignment_assignmentx1); }
      }
      else if (task_taskx2.getassignment().contains(assignment_assignmentx1))
      { assignment_assignmentx1.settask(task_taskx2); } 
    }
  }
  }


  public void saveModel(String file)
  { File outfile = new File(file); 
    PrintWriter out; 
    try { out = new PrintWriter(new BufferedWriter(new FileWriter(outfile))); }
    catch (Exception e) { return; } 
  for (int _i = 0; _i < staffs.size(); _i++)
  { Staff staffx_ = (Staff) staffs.get(_i);
    out.println("staffx_" + _i + " : Staff");
    out.println("staffx_" + _i + ".staffId = \"" + staffx_.getstaffId() + "\"");
    out.println("staffx_" + _i + ".costDay = " + staffx_.getcostDay());
  }

  for (int _i = 0; _i < assignments.size(); _i++)
  { Assignment assignmentx_ = (Assignment) assignments.get(_i);
    out.println("assignmentx_" + _i + " : Assignment");
  }

  for (int _i = 0; _i < tasks.size(); _i++)
  { Task taskx_ = (Task) tasks.get(_i);
    out.println("taskx_" + _i + " : Task");
    out.println("taskx_" + _i + ".taskId = \"" + taskx_.gettaskId() + "\"");
    out.println("taskx_" + _i + ".duration = " + taskx_.getduration());
  }

  for (int _i = 0; _i < allocatestaffs.size(); _i++)
  { AllocateStaff allocatestaffx_ = (AllocateStaff) allocatestaffs.get(_i);
    out.println("allocatestaffx_" + _i + " : AllocateStaff");
  }

  for (int _i = 0; _i < displayschedules.size(); _i++)
  { DisplaySchedule displayschedulex_ = (DisplaySchedule) displayschedules.get(_i);
    out.println("displayschedulex_" + _i + " : DisplaySchedule");
  }

  for (int _i = 0; _i < storys.size(); _i++)
  { Story storyx_ = (Story) storys.get(_i);
    out.println("storyx_" + _i + " : Story");
    out.println("storyx_" + _i + ".storyId = \"" + storyx_.getstoryId() + "\"");
  }

  for (int _i = 0; _i < skills.size(); _i++)
  { Skill skillx_ = (Skill) skills.get(_i);
    out.println("skillx_" + _i + " : Skill");
    out.println("skillx_" + _i + ".skillId = \"" + skillx_.getskillId() + "\"");
  }

  for (int _i = 0; _i < schedules.size(); _i++)
  { Schedule schedulex_ = (Schedule) schedules.get(_i);
    out.println("schedulex_" + _i + " : Schedule");
    out.println("schedulex_" + _i + ".totalCost = " + schedulex_.gettotalCost());
  }

  for (int _i = 0; _i < creates.size(); _i++)
  { Create createx_ = (Create) creates.get(_i);
    out.println("createx_" + _i + " : Create");
  }

  for (int _i = 0; _i < calculatecosts.size(); _i++)
  { CalculateCost calculatecostx_ = (CalculateCost) calculatecosts.get(_i);
    out.println("calculatecostx_" + _i + " : CalculateCost");
  }

  for (int _i = 0; _i < staffs.size(); _i++)
  { Staff staffx_ = (Staff) staffs.get(_i);
    List staff_assigned_Assignment = staffx_.getassigned();
    for (int _j = 0; _j < staff_assigned_Assignment.size(); _j++)
    { out.println("assignmentx_" + assignments.indexOf(staff_assigned_Assignment.get(_j)) + " : staffx_" + _i + ".assigned");
    }
    List staff_has_Skill = staffx_.gethas();
    for (int _j = 0; _j < staff_has_Skill.size(); _j++)
    { out.println("skillx_" + skills.indexOf(staff_has_Skill.get(_j)) + " : staffx_" + _i + ".has");
    }
  }
  for (int _i = 0; _i < assignments.size(); _i++)
  { Assignment assignmentx_ = (Assignment) assignments.get(_i);
    out.println("assignmentx_" + _i + ".staff = staffx_" + staffs.indexOf(((Assignment) assignments.get(_i)).getstaff()));
    out.println("assignmentx_" + _i + ".task = taskx_" + tasks.indexOf(((Assignment) assignments.get(_i)).gettask()));
  }
  for (int _i = 0; _i < tasks.size(); _i++)
  { Task taskx_ = (Task) tasks.get(_i);
    List task_assignment_Assignment = taskx_.getassignment();
    for (int _j = 0; _j < task_assignment_Assignment.size(); _j++)
    { out.println("assignmentx_" + assignments.indexOf(task_assignment_Assignment.get(_j)) + " : taskx_" + _i + ".assignment");
    }
    List task_dependsOn_Task = taskx_.getdependsOn();
    for (int _j = 0; _j < task_dependsOn_Task.size(); _j++)
    { out.println("taskx_" + tasks.indexOf(task_dependsOn_Task.get(_j)) + " : taskx_" + _i + ".dependsOn");
    }
    List task_needs_Skill = taskx_.getneeds();
    for (int _j = 0; _j < task_needs_Skill.size(); _j++)
    { out.println("skillx_" + skills.indexOf(task_needs_Skill.get(_j)) + " : taskx_" + _i + ".needs");
    }
  }
  for (int _i = 0; _i < storys.size(); _i++)
  { Story storyx_ = (Story) storys.get(_i);
    List story_needs_Task = storyx_.getneeds();
    for (int _j = 0; _j < story_needs_Task.size(); _j++)
    { out.println("taskx_" + tasks.indexOf(story_needs_Task.get(_j)) + " : storyx_" + _i + ".needs");
    }
  }
  for (int _i = 0; _i < schedules.size(); _i++)
  { Schedule schedulex_ = (Schedule) schedules.get(_i);
    List schedule_Assignment_Assignment = schedulex_.getAssignment();
    for (int _j = 0; _j < schedule_Assignment_Assignment.size(); _j++)
    { out.println("assignmentx_" + assignments.indexOf(schedule_Assignment_Assignment.get(_j)) + " : schedulex_" + _i + ".Assignment");
    }
  }
    out.close(); 
  }


  public void saveXSI(String file)
  { File outfile = new File(file); 
    PrintWriter out; 
    try { out = new PrintWriter(new BufferedWriter(new FileWriter(outfile))); }
    catch (Exception e) { return; } 
    out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    out.println("<My:model xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\">");
    for (int _i = 0; _i < staffs.size(); _i++)
    { Staff staffx_ = (Staff) staffs.get(_i);
       out.print("<staffs xsi:type=\"My:Staff\"");
    out.print(" staffId=\"" + staffx_.getstaffId() + "\" ");
    out.print(" costDay=\"" + staffx_.getcostDay() + "\" ");
    out.print(" assigned = \"");
    List staff_assigned = staffx_.getassigned();
    for (int _j = 0; _j < staff_assigned.size(); _j++)
    { out.print(" //@assignments." + assignments.indexOf(staff_assigned.get(_j)));
    }
    out.print("\"");
    out.print(" has = \"");
    List staff_has = staffx_.gethas();
    for (int _j = 0; _j < staff_has.size(); _j++)
    { out.print(" //@skills." + skills.indexOf(staff_has.get(_j)));
    }
    out.print("\"");
    out.println(" />");
  }

    for (int _i = 0; _i < assignments.size(); _i++)
    { Assignment assignmentx_ = (Assignment) assignments.get(_i);
       out.print("<assignments xsi:type=\"My:Assignment\"");
    out.print(" staff=\"");
    out.print("//@staffs." + staffs.indexOf(((Assignment) assignments.get(_i)).getstaff()));
    out.print("\"");
    out.print(" task=\"");
    out.print("//@tasks." + tasks.indexOf(((Assignment) assignments.get(_i)).gettask()));
    out.print("\"");
    out.println(" />");
  }

    for (int _i = 0; _i < tasks.size(); _i++)
    { Task taskx_ = (Task) tasks.get(_i);
       out.print("<tasks xsi:type=\"My:Task\"");
    out.print(" taskId=\"" + taskx_.gettaskId() + "\" ");
    out.print(" duration=\"" + taskx_.getduration() + "\" ");
    out.print(" assignment = \"");
    List task_assignment = taskx_.getassignment();
    for (int _j = 0; _j < task_assignment.size(); _j++)
    { out.print(" //@assignments." + assignments.indexOf(task_assignment.get(_j)));
    }
    out.print("\"");
    out.print(" dependsOn = \"");
    List task_dependsOn = taskx_.getdependsOn();
    for (int _j = 0; _j < task_dependsOn.size(); _j++)
    { out.print(" //@tasks." + tasks.indexOf(task_dependsOn.get(_j)));
    }
    out.print("\"");
    out.print(" needs = \"");
    List task_needs = taskx_.getneeds();
    for (int _j = 0; _j < task_needs.size(); _j++)
    { out.print(" //@skills." + skills.indexOf(task_needs.get(_j)));
    }
    out.print("\"");
    out.println(" />");
  }

    for (int _i = 0; _i < allocatestaffs.size(); _i++)
    { AllocateStaff allocatestaffx_ = (AllocateStaff) allocatestaffs.get(_i);
       out.print("<allocatestaffs xsi:type=\"My:AllocateStaff\"");
    out.println(" />");
  }

    for (int _i = 0; _i < displayschedules.size(); _i++)
    { DisplaySchedule displayschedulex_ = (DisplaySchedule) displayschedules.get(_i);
       out.print("<displayschedules xsi:type=\"My:DisplaySchedule\"");
    out.println(" />");
  }

    for (int _i = 0; _i < storys.size(); _i++)
    { Story storyx_ = (Story) storys.get(_i);
       out.print("<storys xsi:type=\"My:Story\"");
    out.print(" storyId=\"" + storyx_.getstoryId() + "\" ");
    out.print(" needs = \"");
    List story_needs = storyx_.getneeds();
    for (int _j = 0; _j < story_needs.size(); _j++)
    { out.print(" //@tasks." + tasks.indexOf(story_needs.get(_j)));
    }
    out.print("\"");
    out.println(" />");
  }

    for (int _i = 0; _i < skills.size(); _i++)
    { Skill skillx_ = (Skill) skills.get(_i);
       out.print("<skills xsi:type=\"My:Skill\"");
    out.print(" skillId=\"" + skillx_.getskillId() + "\" ");
    out.println(" />");
  }

    for (int _i = 0; _i < schedules.size(); _i++)
    { Schedule schedulex_ = (Schedule) schedules.get(_i);
       out.print("<schedules xsi:type=\"My:Schedule\"");
    out.print(" totalCost=\"" + schedulex_.gettotalCost() + "\" ");
    out.print(" Assignment = \"");
    List schedule_Assignment = schedulex_.getAssignment();
    for (int _j = 0; _j < schedule_Assignment.size(); _j++)
    { out.print(" //@assignments." + assignments.indexOf(schedule_Assignment.get(_j)));
    }
    out.print("\"");
    out.println(" />");
  }

    for (int _i = 0; _i < creates.size(); _i++)
    { Create createx_ = (Create) creates.get(_i);
       out.print("<creates xsi:type=\"My:Create\"");
    out.println(" />");
  }

    for (int _i = 0; _i < calculatecosts.size(); _i++)
    { CalculateCost calculatecostx_ = (CalculateCost) calculatecosts.get(_i);
       out.print("<calculatecosts xsi:type=\"My:CalculateCost\"");
    out.println(" />");
  }

    out.println("</My:model>");
    out.close(); 
  }



  public void addStaff(Staff oo) { staffs.add(oo); }

  public Staff getStaffByPK(String staffIdx)
  {  return (Staff) staffstaffIdindex.get(staffIdx); }

  public List getStaffByPK(List staffIdx)
  { Vector res = new Vector(); 
    for (int _i = 0; _i < staffIdx.size(); _i++)
    { Staff staffx = getStaffByPK((String) staffIdx.get(_i));
      if (staffx != null) { res.add(staffx); }
    }
    return res; 
  }

  public void addAssignment(Assignment oo) { assignments.add(oo); }

  public void addTask(Task oo) { tasks.add(oo); }

  public Task getTaskByPK(String taskIdx)
  {  return (Task) tasktaskIdindex.get(taskIdx); }

  public List getTaskByPK(List taskIdx)
  { Vector res = new Vector(); 
    for (int _i = 0; _i < taskIdx.size(); _i++)
    { Task taskx = getTaskByPK((String) taskIdx.get(_i));
      if (taskx != null) { res.add(taskx); }
    }
    return res; 
  }

  public void addAllocateStaff(AllocateStaff oo) { allocatestaffs.add(oo); }

  public void addDisplaySchedule(DisplaySchedule oo) { displayschedules.add(oo); }

  public void addStory(Story oo) { storys.add(oo); }

  public Story getStoryByPK(String storyIdx)
  {  return (Story) storystoryIdindex.get(storyIdx); }

  public List getStoryByPK(List storyIdx)
  { Vector res = new Vector(); 
    for (int _i = 0; _i < storyIdx.size(); _i++)
    { Story storyx = getStoryByPK((String) storyIdx.get(_i));
      if (storyx != null) { res.add(storyx); }
    }
    return res; 
  }

  public void addSkill(Skill oo) { skills.add(oo); }

  public Skill getSkillByPK(String skillIdx)
  {  return (Skill) skillskillIdindex.get(skillIdx); }

  public List getSkillByPK(List skillIdx)
  { Vector res = new Vector(); 
    for (int _i = 0; _i < skillIdx.size(); _i++)
    { Skill skillx = getSkillByPK((String) skillIdx.get(_i));
      if (skillx != null) { res.add(skillx); }
    }
    return res; 
  }

  public void addSchedule(Schedule oo) { schedules.add(oo); }

  public void addCreate(Create oo) { creates.add(oo); }

  public void addCalculateCost(CalculateCost oo) { calculatecosts.add(oo); }



  public void createAllStaff(List staffx)
  { for (int i = 0; i < staffx.size(); i++)
    { Staff staffx_x = (Staff) staffx.get(i);
      if (staffx_x == null) { staffx_x = new Staff(); }
      staffx.set(i,staffx_x);
      addStaff(staffx_x);
    }
  }


  public Staff createStaff(String staffIdx)
  { 
    if (staffstaffIdindex.get(staffIdx) != null) { return null; }
    Staff staffx = new Staff();
    addStaff(staffx);
    setstaffId(staffx,staffIdx);
    setcostDay(staffx,0);
    setassigned(staffx,new Vector());
    sethas(staffx,new Vector());
    staffstaffIdindex.put(staffIdx,staffx);

    return staffx;
  }

  public void createAllAssignment(List assignmentx)
  { for (int i = 0; i < assignmentx.size(); i++)
    { Assignment assignmentx_x = (Assignment) assignmentx.get(i);
      if (assignmentx_x == null) { assignmentx_x = new Assignment(); }
      assignmentx.set(i,assignmentx_x);
      addAssignment(assignmentx_x);
    }
  }


  public Assignment createAssignment(Staff staffx,Task taskx)
  { 
    Assignment assignmentx = new Assignment(staffx,taskx);
    addAssignment(assignmentx);
    setstaff(assignmentx,staffx);
    settask(assignmentx,taskx);

    return assignmentx;
  }

  public void createAllTask(List taskx)
  { for (int i = 0; i < taskx.size(); i++)
    { Task taskx_x = (Task) taskx.get(i);
      if (taskx_x == null) { taskx_x = new Task(); }
      taskx.set(i,taskx_x);
      addTask(taskx_x);
    }
  }


  public Task createTask(String taskIdx)
  { 
    if (tasktaskIdindex.get(taskIdx) != null) { return null; }
    Task taskx = new Task();
    addTask(taskx);
    settaskId(taskx,taskIdx);
    setduration(taskx,0);
    setassignment(taskx,new Vector());
    setdependsOn(taskx,new Vector());
    setneeds(taskx,new Vector());
    tasktaskIdindex.put(taskIdx,taskx);

    return taskx;
  }

  public void createAllAllocateStaff(List allocatestaffx)
  { for (int i = 0; i < allocatestaffx.size(); i++)
    { AllocateStaff allocatestaffx_x = (AllocateStaff) allocatestaffx.get(i);
      if (allocatestaffx_x == null) { allocatestaffx_x = new AllocateStaff(); }
      allocatestaffx.set(i,allocatestaffx_x);
      addAllocateStaff(allocatestaffx_x);
    }
  }


  public AllocateStaff createAllocateStaff()
  { 
    AllocateStaff allocatestaffx = new AllocateStaff();
    addAllocateStaff(allocatestaffx);

    return allocatestaffx;
  }

  public void createAllDisplaySchedule(List displayschedulex)
  { for (int i = 0; i < displayschedulex.size(); i++)
    { DisplaySchedule displayschedulex_x = (DisplaySchedule) displayschedulex.get(i);
      if (displayschedulex_x == null) { displayschedulex_x = new DisplaySchedule(); }
      displayschedulex.set(i,displayschedulex_x);
      addDisplaySchedule(displayschedulex_x);
    }
  }


  public DisplaySchedule createDisplaySchedule()
  { 
    DisplaySchedule displayschedulex = new DisplaySchedule();
    addDisplaySchedule(displayschedulex);

    return displayschedulex;
  }

  public void createAllStory(List storyx)
  { for (int i = 0; i < storyx.size(); i++)
    { Story storyx_x = (Story) storyx.get(i);
      if (storyx_x == null) { storyx_x = new Story(); }
      storyx.set(i,storyx_x);
      addStory(storyx_x);
    }
  }


  public Story createStory(String storyIdx)
  { 
    if (storystoryIdindex.get(storyIdx) != null) { return null; }
    Story storyx = new Story();
    addStory(storyx);
    setstoryId(storyx,storyIdx);
    setneeds(storyx,new Vector());
    storystoryIdindex.put(storyIdx,storyx);

    return storyx;
  }

  public void createAllSkill(List skillx)
  { for (int i = 0; i < skillx.size(); i++)
    { Skill skillx_x = (Skill) skillx.get(i);
      if (skillx_x == null) { skillx_x = new Skill(); }
      skillx.set(i,skillx_x);
      addSkill(skillx_x);
    }
  }


  public Skill createSkill(String skillIdx)
  { 
    if (skillskillIdindex.get(skillIdx) != null) { return null; }
    Skill skillx = new Skill();
    addSkill(skillx);
    setskillId(skillx,skillIdx);
    skillskillIdindex.put(skillIdx,skillx);

    return skillx;
  }

  public void createAllSchedule(List schedulex)
  { for (int i = 0; i < schedulex.size(); i++)
    { Schedule schedulex_x = (Schedule) schedulex.get(i);
      if (schedulex_x == null) { schedulex_x = new Schedule(); }
      schedulex.set(i,schedulex_x);
      addSchedule(schedulex_x);
    }
  }


  public Schedule createSchedule()
  { 
    Schedule schedulex = new Schedule();
    addSchedule(schedulex);
    settotalCost(schedulex,0);
    setAssignment(schedulex,new Vector());

    return schedulex;
  }

  public void createAllCreate(List createx)
  { for (int i = 0; i < createx.size(); i++)
    { Create createx_x = (Create) createx.get(i);
      if (createx_x == null) { createx_x = new Create(); }
      createx.set(i,createx_x);
      addCreate(createx_x);
    }
  }


  public Create createCreate()
  { 
    Create createx = new Create();
    addCreate(createx);

    return createx;
  }

  public void createAllCalculateCost(List calculatecostx)
  { for (int i = 0; i < calculatecostx.size(); i++)
    { CalculateCost calculatecostx_x = (CalculateCost) calculatecostx.get(i);
      if (calculatecostx_x == null) { calculatecostx_x = new CalculateCost(); }
      calculatecostx.set(i,calculatecostx_x);
      addCalculateCost(calculatecostx_x);
    }
  }


  public CalculateCost createCalculateCost()
  { 
    CalculateCost calculatecostx = new CalculateCost();
    addCalculateCost(calculatecostx);

    return calculatecostx;
  }


public void setstaffId(Staff staffx, String staffId_x) 
  { if (staffstaffIdindex.get(staffId_x) != null) { return; }
  staffstaffIdindex.remove(staffx.getstaffId());
  staffx.setstaffId(staffId_x);
  staffstaffIdindex.put(staffId_x,staffx);
    }


public void setcostDay(Staff staffx, int costDay_x) 
  { staffx.setcostDay(costDay_x);
    }


  public void setassigned(Staff staffx, List assignedxx) 
  {   List _oldassignedxx = staffx.getassigned();
    for (int _j = 0; _j < _oldassignedxx.size(); _j++)
    { Assignment _yy = (Assignment) _oldassignedxx.get(_j);
      if (assignedxx.contains(_yy)) { }
      else { _yy.setstaff(null); }
    }
  for (int _i = 0; _i < assignedxx.size(); _i++)
  { Assignment _xx = (Assignment) assignedxx.get(_i);
    if (_oldassignedxx.contains(_xx)) { }
    else { if (_xx.getstaff() != null) { _xx.getstaff().removeassigned(_xx); }  }
    _xx.setstaff(staffx);
  }
    staffx.setassigned(assignedxx);
      }


  public void addassigned(Staff staffx, Assignment assignedxx) 
  { if (staffx.getassigned().contains(assignedxx)) { return; }
    if (assignedxx.getstaff() != null) { assignedxx.getstaff().removeassigned(assignedxx); }
  assignedxx.setstaff(staffx);
    staffx.addassigned(assignedxx);
    }


  public void removeassigned(Staff staffx, Assignment assignedxx) 
  { staffx.removeassigned(assignedxx);
    assignedxx.setstaff(null);
  }


 public void unionassigned(Staff staffx,List assignedx)
  { for (int _i = 0; _i < assignedx.size(); _i++)
    { Assignment assignmentxassigned = (Assignment) assignedx.get(_i);
      addassigned(staffx,assignmentxassigned);
     } } 


 public void subtractassigned(Staff staffx,List assignedx)
  { for (int _i = 0; _i < assignedx.size(); _i++)
    { Assignment assignmentxassigned = (Assignment) assignedx.get(_i);
      removeassigned(staffx,assignmentxassigned);
     } } 


  public void sethas(Staff staffx, List hasxx) 
  {   List _oldhasxx = staffx.gethas();
  for (int _i = 0; _i < hasxx.size(); _i++)
  { Skill _xx = (Skill) hasxx.get(_i);
    if (_oldhasxx.contains(_xx)) { }
    else { Staff.removeAllhas(staffs, _xx); }
  }
    staffx.sethas(hasxx);
      }


  public void addhas(Staff staffx, Skill hasxx) 
  { if (staffx.gethas().contains(hasxx)) { return; }
    Staff.removeAllhas(staffs,hasxx);
    staffx.addhas(hasxx);
    }


  public void removehas(Staff staffx, Skill hasxx) 
  { staffx.removehas(hasxx);
    }


 public void unionhas(Staff staffx,List hasx)
  { for (int _i = 0; _i < hasx.size(); _i++)
    { Skill skillxhas = (Skill) hasx.get(_i);
      addhas(staffx,skillxhas);
     } } 


 public void subtracthas(Staff staffx,List hasx)
  { for (int _i = 0; _i < hasx.size(); _i++)
    { Skill skillxhas = (Skill) hasx.get(_i);
      removehas(staffx,skillxhas);
     } } 


  public void setstaff(Assignment assignmentx, Staff staffxx) 
  {   if (assignmentx.getstaff() == staffxx) { return; }
    if (assignmentx.getstaff() != null)
    { Staff old_value = assignmentx.getstaff();
      old_value.removeassigned(assignmentx); } 
    staffxx.addassigned(assignmentx);
    assignmentx.setstaff(staffxx);
      }


  public void settask(Assignment assignmentx, Task taskxx) 
  {   if (assignmentx.gettask() == taskxx) { return; }
    if (assignmentx.gettask() != null)
    { Task old_value = assignmentx.gettask();
      old_value.removeassignment(assignmentx); } 
    taskxx.addassignment(assignmentx);
    assignmentx.settask(taskxx);
      }


public void settaskId(Task taskx, String taskId_x) 
  { if (tasktaskIdindex.get(taskId_x) != null) { return; }
  tasktaskIdindex.remove(taskx.gettaskId());
  taskx.settaskId(taskId_x);
  tasktaskIdindex.put(taskId_x,taskx);
    }


public void setduration(Task taskx, int duration_x) 
  { taskx.setduration(duration_x);
    }


  public void setassignment(Task taskx, List assignmentxx) 
  {   List _oldassignmentxx = taskx.getassignment();
    for (int _j = 0; _j < _oldassignmentxx.size(); _j++)
    { Assignment _yy = (Assignment) _oldassignmentxx.get(_j);
      if (assignmentxx.contains(_yy)) { }
      else { _yy.settask(null); }
    }
  for (int _i = 0; _i < assignmentxx.size(); _i++)
  { Assignment _xx = (Assignment) assignmentxx.get(_i);
    if (_oldassignmentxx.contains(_xx)) { }
    else { if (_xx.gettask() != null) { _xx.gettask().removeassignment(_xx); }  }
    _xx.settask(taskx);
  }
    taskx.setassignment(assignmentxx);
      }


  public void addassignment(Task taskx, Assignment assignmentxx) 
  { if (taskx.getassignment().contains(assignmentxx)) { return; }
    if (assignmentxx.gettask() != null) { assignmentxx.gettask().removeassignment(assignmentxx); }
  assignmentxx.settask(taskx);
    taskx.addassignment(assignmentxx);
    }


  public void removeassignment(Task taskx, Assignment assignmentxx) 
  { taskx.removeassignment(assignmentxx);
    assignmentxx.settask(null);
  }


 public void unionassignment(Task taskx,List assignmentx)
  { for (int _i = 0; _i < assignmentx.size(); _i++)
    { Assignment assignmentxassignment = (Assignment) assignmentx.get(_i);
      addassignment(taskx,assignmentxassignment);
     } } 


 public void subtractassignment(Task taskx,List assignmentx)
  { for (int _i = 0; _i < assignmentx.size(); _i++)
    { Assignment assignmentxassignment = (Assignment) assignmentx.get(_i);
      removeassignment(taskx,assignmentxassignment);
     } } 


  public void setdependsOn(Task taskx, List dependsOnxx) 
  {     taskx.setdependsOn(dependsOnxx);
      }


  public void adddependsOn(Task taskx, Task dependsOnxx) 
  { if (taskx.getdependsOn().contains(dependsOnxx)) { return; }
      taskx.adddependsOn(dependsOnxx);
    }


  public void removedependsOn(Task taskx, Task dependsOnxx) 
  { taskx.removedependsOn(dependsOnxx);
    }


 public void uniondependsOn(Task taskx,List dependsOnx)
  { for (int _i = 0; _i < dependsOnx.size(); _i++)
    { Task taskxdependsOn = (Task) dependsOnx.get(_i);
      adddependsOn(taskx,taskxdependsOn);
     } } 


 public void subtractdependsOn(Task taskx,List dependsOnx)
  { for (int _i = 0; _i < dependsOnx.size(); _i++)
    { Task taskxdependsOn = (Task) dependsOnx.get(_i);
      removedependsOn(taskx,taskxdependsOn);
     } } 


  public void setneeds(Task taskx, List needsxx) 
  {   List _oldneedsxx = taskx.getneeds();
  for (int _i = 0; _i < needsxx.size(); _i++)
  { Skill _xx = (Skill) needsxx.get(_i);
    if (_oldneedsxx.contains(_xx)) { }
    else { Task.removeAllneeds(tasks, _xx); }
  }
    taskx.setneeds(needsxx);
      }


  public void addneeds(Task taskx, Skill needsxx) 
  { if (taskx.getneeds().contains(needsxx)) { return; }
    Task.removeAllneeds(tasks,needsxx);
    taskx.addneeds(needsxx);
    }


  public void removeneeds(Task taskx, Skill needsxx) 
  { taskx.removeneeds(needsxx);
    }


 public void unionneeds(Task taskx,List needsx)
  { for (int _i = 0; _i < needsx.size(); _i++)
    { Skill skillxneeds = (Skill) needsx.get(_i);
      addneeds(taskx,skillxneeds);
     } } 


 public void subtractneeds(Task taskx,List needsx)
  { for (int _i = 0; _i < needsx.size(); _i++)
    { Skill skillxneeds = (Skill) needsx.get(_i);
      removeneeds(taskx,skillxneeds);
     } } 


public void setstoryId(Story storyx, String storyId_x) 
  { if (storystoryIdindex.get(storyId_x) != null) { return; }
  storystoryIdindex.remove(storyx.getstoryId());
  storyx.setstoryId(storyId_x);
  storystoryIdindex.put(storyId_x,storyx);
    }


  public void setneeds(Story storyx, List needsxx) 
  {   List _oldneedsxx = storyx.getneeds();
  for (int _i = 0; _i < needsxx.size(); _i++)
  { Task _xx = (Task) needsxx.get(_i);
    if (_oldneedsxx.contains(_xx)) { }
    else { Story.removeAllneeds(storys, _xx); }
  }
    storyx.setneeds(needsxx);
      }


  public void addneeds(Story storyx, Task needsxx) 
  { if (storyx.getneeds().contains(needsxx)) { return; }
    Story.removeAllneeds(storys,needsxx);
    storyx.addneeds(needsxx);
    }


  public void removeneeds(Story storyx, Task needsxx) 
  { storyx.removeneeds(needsxx);
    }


 public void unionneeds(Story storyx,List needsx)
  { for (int _i = 0; _i < needsx.size(); _i++)
    { Task taskxneeds = (Task) needsx.get(_i);
      addneeds(storyx,taskxneeds);
     } } 


 public void subtractneeds(Story storyx,List needsx)
  { for (int _i = 0; _i < needsx.size(); _i++)
    { Task taskxneeds = (Task) needsx.get(_i);
      removeneeds(storyx,taskxneeds);
     } } 


public void setskillId(Skill skillx, String skillId_x) 
  { if (skillskillIdindex.get(skillId_x) != null) { return; }
  skillskillIdindex.remove(skillx.getskillId());
  skillx.setskillId(skillId_x);
  skillskillIdindex.put(skillId_x,skillx);
    }


public void settotalCost(Schedule schedulex, int totalCost_x) 
  { schedulex.settotalCost(totalCost_x);
    }


  public void setAssignment(Schedule schedulex, List Assignmentxx) 
  {   List _oldAssignmentxx = schedulex.getAssignment();
  for (int _i = 0; _i < Assignmentxx.size(); _i++)
  { Assignment _xx = (Assignment) Assignmentxx.get(_i);
    if (_oldAssignmentxx.contains(_xx)) { }
    else { Schedule.removeAllAssignment(schedules, _xx); }
  }
    schedulex.setAssignment(Assignmentxx);
      }


  public void setAssignment(Schedule schedulex, int _ind, Assignment assignmentx) 
  { schedulex.setAssignment(_ind,assignmentx); }
  
  public void addAssignment(Schedule schedulex, Assignment Assignmentxx) 
  {   Schedule.removeAllAssignment(schedules,Assignmentxx);
    schedulex.addAssignment(Assignmentxx);
    }


  public void removeAssignment(Schedule schedulex, Assignment Assignmentxx) 
  { schedulex.removeAssignment(Assignmentxx);
    }


 public void unionAssignment(Schedule schedulex,List Assignmentx)
  { for (int _i = 0; _i < Assignmentx.size(); _i++)
    { Assignment assignmentxAssignment = (Assignment) Assignmentx.get(_i);
      addAssignment(schedulex,assignmentxAssignment);
     } } 


 public void subtractAssignment(Schedule schedulex,List Assignmentx)
  { for (int _i = 0; _i < Assignmentx.size(); _i++)
    { Assignment assignmentxAssignment = (Assignment) Assignmentx.get(_i);
      removeAssignment(schedulex,assignmentxAssignment);
     } } 



  public  List AllAssignmenttoString(List assignmentxs)
  { 
    List result = new Vector();
    for (int _i = 0; _i < assignmentxs.size(); _i++)
    { Assignment assignmentx = (Assignment) assignmentxs.get(_i);
      result.add(assignmentx.toString());
    }
    return result; 
  }

  public void displayschedule1(Assignment assignmentx)
  {   assignmentx.displayschedule1();
   }

  public void allocatestaff1(Task taskx,Staff st)
  {   //  if (!(taskx.getassignment().size() == 0 && st.getassigned().size() == 0)) { return; } 
    taskx.allocatestaff1(st);
   }

  public  List AllTaskallocatestaff1test(List taskxs,Staff st)
  { 
    List result = new Vector();
    for (int _i = 0; _i < taskxs.size(); _i++)
    { Task taskx = (Task) taskxs.get(_i);
      result.add(new Boolean(taskx.allocatestaff1test(st)));
    }
    return result; 
  }

 public static boolean allocatestaff1search()
 { return Task.allocatestaff1search(); }



  public void killAllStaff(List staffxx)
  { for (int _i = 0; _i < staffxx.size(); _i++)
    { killStaff((Staff) staffxx.get(_i)); }
  }

  public void killStaff(Staff staffxx)
  { if (staffxx == null) { return; }
   staffs.remove(staffxx);
    Vector _1removedstaffAssignment = new Vector();
    Vector _1qrangestaffAssignment = new Vector();
    _1qrangestaffAssignment.addAll(staffxx.getassigned());
    for (int _i = 0; _i < _1qrangestaffAssignment.size(); _i++)
    { Assignment assignmentx = (Assignment) _1qrangestaffAssignment.get(_i);
      if (assignmentx != null && staffxx.equals(assignmentx.getstaff()))
      { _1removedstaffAssignment.add(assignmentx);
        assignmentx.setstaff(null);
      }
    }
    staffstaffIdindex.remove(staffxx.getstaffId());
    for (int _i = 0; _i < _1removedstaffAssignment.size(); _i++)
    { killAssignment((Assignment) _1removedstaffAssignment.get(_i)); }
  }



  public void killAllAssignment(List assignmentxx)
  { for (int _i = 0; _i < assignmentxx.size(); _i++)
    { killAssignment((Assignment) assignmentxx.get(_i)); }
  }

  public void killAssignment(Assignment assignmentxx)
  { if (assignmentxx == null) { return; }
   assignments.remove(assignmentxx);
    Vector _2qrangeassignedStaff = new Vector();
    _2qrangeassignedStaff.add(assignmentxx.getstaff());
    for (int _i = 0; _i < _2qrangeassignedStaff.size(); _i++)
    { Staff staffx = (Staff) _2qrangeassignedStaff.get(_i);
      if (staffx != null && staffx.getassigned().contains(assignmentxx))
      { removeassigned(staffx,assignmentxx); }
    }
    Vector _2qrangeassignmentTask = new Vector();
    _2qrangeassignmentTask.add(assignmentxx.gettask());
    for (int _i = 0; _i < _2qrangeassignmentTask.size(); _i++)
    { Task taskx = (Task) _2qrangeassignmentTask.get(_i);
      if (taskx != null && taskx.getassignment().contains(assignmentxx))
      { removeassignment(taskx,assignmentxx); }
    }
    Vector _1qrangeAssignmentSchedule = new Vector();
    _1qrangeAssignmentSchedule.addAll(schedules);
    for (int _i = 0; _i < _1qrangeAssignmentSchedule.size(); _i++)
    { Schedule schedulex = (Schedule) _1qrangeAssignmentSchedule.get(_i);
      if (schedulex != null && schedulex.getAssignment().contains(assignmentxx))
      { removeAssignment(schedulex,assignmentxx); }
    }
  }



  public void killAllTask(List taskxx)
  { for (int _i = 0; _i < taskxx.size(); _i++)
    { killTask((Task) taskxx.get(_i)); }
  }

  public void killTask(Task taskxx)
  { if (taskxx == null) { return; }
   tasks.remove(taskxx);
    Vector _1removedtaskAssignment = new Vector();
    Vector _1qrangetaskAssignment = new Vector();
    _1qrangetaskAssignment.addAll(taskxx.getassignment());
    for (int _i = 0; _i < _1qrangetaskAssignment.size(); _i++)
    { Assignment assignmentx = (Assignment) _1qrangetaskAssignment.get(_i);
      if (assignmentx != null && taskxx.equals(assignmentx.gettask()))
      { _1removedtaskAssignment.add(assignmentx);
        assignmentx.settask(null);
      }
    }
    Vector _1qrangedependsOnTask = new Vector();
    _1qrangedependsOnTask.addAll(tasks);
    for (int _i = 0; _i < _1qrangedependsOnTask.size(); _i++)
    { Task taskx = (Task) _1qrangedependsOnTask.get(_i);
      if (taskx != null && taskx.getdependsOn().contains(taskxx))
      { removedependsOn(taskx,taskxx); }
    }
    Vector _1qrangeneedsStory = new Vector();
    _1qrangeneedsStory.addAll(storys);
    for (int _i = 0; _i < _1qrangeneedsStory.size(); _i++)
    { Story storyx = (Story) _1qrangeneedsStory.get(_i);
      if (storyx != null && storyx.getneeds().contains(taskxx))
      { removeneeds(storyx,taskxx); }
    }
    tasktaskIdindex.remove(taskxx.gettaskId());
    for (int _i = 0; _i < _1removedtaskAssignment.size(); _i++)
    { killAssignment((Assignment) _1removedtaskAssignment.get(_i)); }
  }



  public void killAllAllocateStaff(List allocatestaffxx)
  { for (int _i = 0; _i < allocatestaffxx.size(); _i++)
    { killAllocateStaff((AllocateStaff) allocatestaffxx.get(_i)); }
  }

  public void killAllocateStaff(AllocateStaff allocatestaffxx)
  { if (allocatestaffxx == null) { return; }
   allocatestaffs.remove(allocatestaffxx);
  }



  public void killAllDisplaySchedule(List displayschedulexx)
  { for (int _i = 0; _i < displayschedulexx.size(); _i++)
    { killDisplaySchedule((DisplaySchedule) displayschedulexx.get(_i)); }
  }

  public void killDisplaySchedule(DisplaySchedule displayschedulexx)
  { if (displayschedulexx == null) { return; }
   displayschedules.remove(displayschedulexx);
  }



  public void killAllStory(List storyxx)
  { for (int _i = 0; _i < storyxx.size(); _i++)
    { killStory((Story) storyxx.get(_i)); }
  }

  public void killStory(Story storyxx)
  { if (storyxx == null) { return; }
   storys.remove(storyxx);
    storystoryIdindex.remove(storyxx.getstoryId());
  }



  public void killAllSkill(List skillxx)
  { for (int _i = 0; _i < skillxx.size(); _i++)
    { killSkill((Skill) skillxx.get(_i)); }
  }

  public void killSkill(Skill skillxx)
  { if (skillxx == null) { return; }
   skills.remove(skillxx);
    Vector _1qrangeneedsTask = new Vector();
    _1qrangeneedsTask.addAll(tasks);
    for (int _i = 0; _i < _1qrangeneedsTask.size(); _i++)
    { Task taskx = (Task) _1qrangeneedsTask.get(_i);
      if (taskx != null && taskx.getneeds().contains(skillxx))
      { removeneeds(taskx,skillxx); }
    }
    Vector _1qrangehasStaff = new Vector();
    _1qrangehasStaff.addAll(staffs);
    for (int _i = 0; _i < _1qrangehasStaff.size(); _i++)
    { Staff staffx = (Staff) _1qrangehasStaff.get(_i);
      if (staffx != null && staffx.gethas().contains(skillxx))
      { removehas(staffx,skillxx); }
    }
    skillskillIdindex.remove(skillxx.getskillId());
  }



  public void killAllSchedule(List schedulexx)
  { for (int _i = 0; _i < schedulexx.size(); _i++)
    { killSchedule((Schedule) schedulexx.get(_i)); }
  }

  public void killSchedule(Schedule schedulexx)
  { if (schedulexx == null) { return; }
   schedules.remove(schedulexx);
  }



  public void killAllCreate(List createxx)
  { for (int _i = 0; _i < createxx.size(); _i++)
    { killCreate((Create) createxx.get(_i)); }
  }

  public void killCreate(Create createxx)
  { if (createxx == null) { return; }
   creates.remove(createxx);
  }



  public void killAllCalculateCost(List calculatecostxx)
  { for (int _i = 0; _i < calculatecostxx.size(); _i++)
    { killCalculateCost((CalculateCost) calculatecostxx.get(_i)); }
  }

  public void killCalculateCost(CalculateCost calculatecostxx)
  { if (calculatecostxx == null) { return; }
   calculatecosts.remove(calculatecostxx);
  }




  
    public void allocateStaff() 
  { 

    boolean allocatestaff1_running;
  allocatestaff1_running = true;
    while (allocatestaff1_running) 
  { allocatestaff1_running = Task.allocatestaff1search(); }


  }



    public void displaySchedule() 
  { 

       List assignmentdisplayschedule1x = new Vector();
  assignmentdisplayschedule1x.addAll(Controller.inst().assignments);
  for (int assignmentdisplayschedule1x_ind4 = 0; assignmentdisplayschedule1x_ind4 < assignmentdisplayschedule1x.size(); assignmentdisplayschedule1x_ind4++)
  { Controller.inst().displayschedule1((Assignment) assignmentdisplayschedule1x.get(assignmentdisplayschedule1x_ind4)); }


  }



    public void calculateCost() 
  { 

  
  }


 
}



